# OpenML dataset: DEE

https://www.openml.org/d/42360

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Daily electric energy dataset

The dee problem involves predicting the daily average price of TkWhe electricity energy in Spain. The data set contains real values from 2003 about the daily consumption in Spain of energy from hydroelectric, nuclear electric, carbon, fuel, natural gas and other special sources of energy.

Missing values. No

### Attributes
1. Hydroelectric real[27881.8,206035.0]
2. Nuclear real[114760.0,187105.0]
3. Coal real[33537.0,234833.0]
4. Fuel real[0.0,67986.5]
5. Gas real[0.0,84452.2]
6. Special real[5307.0,16357.0]
7. Consume real[0.765853,5.11875]

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42360) of an [OpenML dataset](https://www.openml.org/d/42360). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42360/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42360/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42360/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

